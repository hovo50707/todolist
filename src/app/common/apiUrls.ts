export const ApiUrls = {
  getGroupId: `https://avetiq-test.firebaseapp.com/group/hovhannes_baghdasaryan`,
  getUserId: `https://avetiq-test.firebaseapp.com/user/hovhannes_baghdasaryan`,
  getList: (groupId,userId)=>{return `https://avetiq-test.firebaseapp.com/proscons/group/${groupId}/user/${userId}`}
};
